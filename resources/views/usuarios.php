<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale=1.0 minimal-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie-edge">
	<title>CursoStyde</title>
</head>
<body>
		<h1><?php echo ($title); ?></h1>
		<ul>
			<?php foreach ($users as $user): ?>
				<li><?php echo ($user); ?></li>
			<?php endforeach; ?>
		</ul>

		<hr>
</body>
</html>