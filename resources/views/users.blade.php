<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale=1.0 minimal-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie-edge">
	<title>Curso Styde</title>
</head>
<body>

		<h1>{{ $title }}</h1>
		
		<hr>

	<!-- Usando el ciclo if -->
	{{-- @if (! empty($users))

		<ul>
			@foreach ($users as $user)
				<li>{{ $user->name }}</li>
			@endforeach
		</ul>

	@else 

		<h2>No hay Usuarios Registrados.</h2>

	@endif --}}
	<!-- Usando el ciclo else -->
	<ul>
			@forelse ($users as $user)
				<li>{{ $user->name }}</li>
			@empty
				<h2>No hay Usuarios Registrados.</h2>
			@endforelse
		</ul>

</body>
</html>