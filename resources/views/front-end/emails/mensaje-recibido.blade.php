<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Mensaje recibido</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<p>Recibiste un Mensaje de: {{ $mensaje['firstName'] }} - {{ $mensaje['email'] }}</p>
	<p><strong>Asunto:</strong> {{ $mensaje['subject'] }}</p>
	<p><strong>Mensaje:</strong> {{ $mensaje['content'] }}</p>
	{{-- {{ var_dump($errors->any()) }} --}}
	{{-- {{ var_dump($mensaje) }} --}}
</body>
</html>