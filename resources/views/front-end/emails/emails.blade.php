@extends('plantillas.plantillaShort1')

@section('title', 'Emails')

@section('content')

  <div class="row">
    {{ $errors->any() == 'true' ? "Hay Errores" : "" }} <!-- Muestra un booleano TRUE si hay errores-->
    <div class="col-lg-12 col-md-12 ml-auto mr-auto">
      <div class="row">
        <div class="col-md-12 text-right">
        </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12">
      <div class="card">
        <div class="card-header card-header-warning">
          <h4 class="card-title">{{ $title }}</h4>
          <p class="card-category">Enviele un correo al Administrador comentandole su opinion</p>
        </div>

        <div class="card-body">

          <form action="{{ route('emails.send') }}" method="POST" role="form">
            @csrf <!-- envio obligatorio del token de seguridad -->
            <div class="row">
              <div class="col-12 col-md-3 col-sm-6 col-xs-6">
                <div class="form-group">
                  <label class="bmd-label-floating">Nombre:</label>
                  <input type="text" name="firstName" class="form-control">
                </div>
                <p class="text-danger"><small>{{ $errors->first('firstName') }}</small></p>
              </div>

              <div class="w-100"></div><!--es lo mismo que el clearfix en bt3 -->

              <div class="col-12 col-md-4 col-sm-6 col-xs-6">
                <div class="form-group">
                  <label class="bmd-label-floating">Email Address</label>
                  <input type="email" name="email" class="form-control">
                </div>
                <p class="text-danger"><small>{{ $errors->first('email') }}</small></p>
              </div>

              <div class="w-100"></div><!--es lo mismo que el clearfix en bt3 -->

              <div class="col-12 col-md-3 col-sm-6 col-xs-6">
                <div class="form-group">
                  <label class="bmd-label-floating">Asunto:</label>
                  <input type="text" name="subject" class="form-control">
                </div>
                <p class="text-danger"><small>{{ $errors->first('subject') }}</small></p>
              </div>

              <div class="w-100"></div><!--es lo mismo que el clearfix en bt3 -->

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label class="bmd-label-floating">Mensaje:</label>
                  <textarea type="text" name="content" class="form-control"></textarea>
                </div>
                <p class="text-danger"><small>{{ $errors->first('content') }}</small></p>
              </div>
            </div>
            <button type="submit" class="btn btn-warning pull-right col-auto">Enviar</button>
          </form>

        </div>

      </div>
    </div>

  </div>
  <!-- /page content -->

@endsection