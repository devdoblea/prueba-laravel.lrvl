@extends('plantillas.plantillaShort1')

@section('title', 'Usuarios')

@section('content')

  <div class="row">

    <div class="col-lg-12 col-md-12 ml-auto mr-auto">
      <div class="row">
        <div class="col-md-12 text-right">
          <button id="agregar" class="btn btn-success btn-round btn-just-icon" rel="tooltip" title="Agregar Usuario" data-toggle="modal" data-target="#ventanaAgregar">
            <i class="material-icons">add</i>
          </button>
        </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12">
      <div class="card">
        <div class="card-header card-header-info">
          <h4 class="card-title">{{ $title }}</h4>
          <p class="card-category">Nuevos empleados a la fecha</p>
        </div>
        <div class="card-body table-responsive">
          <table class="table table-hover">
            <thead class="text-warning">
              <th class="text-center">ID</th>
              <th class="text-left" > Nombres</th>
              <th class="text-center">Correo</th>
              <th class="text-center">Cargo</th>
              <th class="text-center" colspan="2">Accion</th>
            </thead>
            <tbody>
                @forelse ($users as $user)
                  <tr>
                    <td class="text-center">{{ $user->id }}</td>
                    <td class="text-left">  {{ $user->firstName . ' ' . $user->lastName }}</td>
                    <td class="text-center">{{ $user->email }}</td>
                    <td class="text-center">{{ $user->profession['title'] }}</td>
                    <td class="td-actions text-center">
                      <!-- tag "a" con url arbitrarias -->
                      <a href="{{ route('usuarios.modaleditusuario') }}" rel="tooltip" title="Edita Usuario" class="btn btn-primary btn-link btn-sm" data-toggle="modal" data-target="#ventanaEditar" data-iduser="{{ $user->id }}">
                        <i class="material-icons">edit</i>
                      </a>
                    </td>
                    <td class="td-actions text-center">
                      <a href="{{ route('usuarios.modaldelusuario') }}" rel="tooltip" title="Remover Usuario" class="btn btn-danger btn-link btn-sm" data-toggle="modal" data-target="#ventanaBorrar" data-iduser="{{ $user->id }}">
                        <i class="material-icons">close</i>
                      </a>
                    </td>
                  </tr>
                @empty
                  <tr>
                    <td colspan="5">No hay Usuarios Registrados.</td>
                  </tr>
                @endforelse
            </tbody>
          </table>
        </div>
        {{ $users->links() }}
      </div>
    </div>

  </div>
  <!-- /page content -->

  <!-- Con este grupo de divs se abre la ventana modal que traera la info a editar del estudiante-->
  <div class="modal fade" id="ventanaAgregar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content ct">
      </div>
    </div>
  </div>
  <!-- Con este grupo de divs se abre la ventana modal que traera la info a editar del estudiante-->
  <div class="modal fade" id="ventanaEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content ct">
      </div>
    </div>
  </div>
  <!-- Con este grupo de divs se abre la ventana modal que traera la info a editar del estudiante-->
  <div class="modal fade" id="ventanaBorrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content ct">
      </div>
    </div>
  </div>

  <script>
    /*$.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });*/

    $('#agregar').on("click", function() {
      var modal = $("#ventanaAgregar").on("show.bs.modal");
      $.ajax({
          url: "{{ route('usuarios.modalregusuario') }}",
          type: "GET",
          success: function (data) {
            //console.log(data);
            modal.find('.ct').html(data);
          },
          error: function(err) {
            alert('ventanaAgregar: '+JSON.stringify(err['statusText']));
          }
      });
    });
  </script>
  <script>
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    // cuando haga clic en el boton editar
    $('#ventanaEditar').on('show.bs.modal', function (e) {
      var button = $(e.relatedTarget); // Button that triggered the modal
      var idus   = button.data("iduser");// Extract info from data-* attributes
      var modal  = $(this);
      var dataString = {idus: idus};
        $.ajax({
          type: "POST",
          url: "{{ route('usuarios.modaleditusuario') }}",
          data: dataString,
          success: function (data) {
            //console.log(data);
            modal.find('.ct').html(data);
          },
          error: function(err) {
            alert('ventanaEditar: '+JSON.stringify(err['statusText']));
          }
        });
    });
  </script>
  <script>
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $('#ventanaBorrar').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var idus   = button.data("iduser");// Extract info from data-* attributes
      var modal  = $(this);
      var dataString = {idus: idus};
      $.ajax({
        type: 'POST',
        url: "{{ route('usuarios.modaldelusuario') }}",
        data: dataString,
          success: function(data) {
            //console.log(data);
            modal.find('.ct').html(data);
          },
          error: function(err) {
            alert('Borrar: '+JSON.stringify(err['statusText']));
          }
      });
    });
  </script>

@endsection