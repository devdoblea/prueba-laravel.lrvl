@extends('plantillas.plantillaShort1')

@section('title', 'Edita Usuario')

@section('content')

  <div class="row">
    <div class="col-md-8">
      <div class="card">
        <!-- encabezado modal -->
        <div class="card-header card-header-primary">
          <h4 class="card-title">Edit Profile</h4>
          <p class="card-category">Complete your profile</p>
        </div>
        <!-- END Encabezado modal -->
        <!-- Cuerpo Modal -->
        <div class="card-body">
          <form>
            @forelse ($users as $user)
              <div class="row">
                <div class="col-md-5">
                  {{-- <div class="form-group">
                    <label class="bmd-label-floating">Company (disabled)</label>
                    <input type="text" class="form-control" disabled>
                  </div> --}}
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label class="bmd-label-floating">Username</label>
                    <input type="text" class="form-control" value="{{ $user->username }}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="bmd-label-floating">Email address</label>
                    <input type="email" class="form-control" value="{{ $user->email }}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Fist Name</label>
                    <input type="text" class="form-control" value="{{ $user->firstName }}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Last Name</label>
                    <input type="text" class="form-control" value="{{ $user->lastName }}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Adress</label>
                    <input type="text" class="form-control" value="{{ $user->address }}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Profesión</label>
                    <select name="profession_id" class="form-control">
                      <option value="{{ $user->profession_id }}">{{ $user->profession->title }}</option>
                      @foreach ($profes as $prof)
                        <option value="{{ $prof->id }}">{{ $prof->title }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Habilidades</label>
                    <select name="skill_id" class="form-control">
                      <option value="{{ $user->skill_id }}">{{ $user->skill->skill }}</option>
                      @foreach ($skills as $skill)
                        <option value="{{ $skill->id }}">{{ $skill->skill }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="bmd-label-floating">Es Administrador?</label>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="isAdmin" <?php if($user->isAdmin == 1){ echo 'checked="checked"';} ?> />Si
                        <span class="form-check-sign">
                          <span class="check"></span>
                        </span>
                      </label>
                      <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="isAdmin" <?php if($user->isAdmin == 0){ echo 'checked="checked"';} ?> />No
                        <span class="form-check-sign">
                          <span class="check"></span>
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Acerca de Mi</label>
                    <div class="form-group">
                      <label class="bmd-label-floating"></label>
                      <textarea class="form-control" rows="5">Website: {{ $user->website }}</textarea>
                    </div>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Actualizar Perfil</button>
              <div class="clearfix"></div>
            @empty
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="bmd-label-floating">No existe este Registro.</label>
                  </div>
                </div>
              </div>
            @endforelse
          </form>
        </div>
        <!-- END Cuerpo Modal -->
      </div>
    </div>

    <!-- perfil del dueño -->
    {{-- <div class="col-md-4">
      <div class="card card-profile">
        <div class="card-avatar">
          <a href="#pablo">
            <img class="img" src="../assets/img/faces/marc.jpg" />
          </a>
        </div>
        <div class="card-body">
          <h6 class="card-category text-gray">CEO / Co-Founder</h6>
          <h4 class="card-title">Alec Thompson</h4>
          <p class="card-description">
            Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
          </p>
          <a href="#pablo" class="btn btn-primary btn-round">Follow</a>
        </div>
      </div>
    </div> --}}
    <!-- END perfil del dueño -->

  </div>

@endsection