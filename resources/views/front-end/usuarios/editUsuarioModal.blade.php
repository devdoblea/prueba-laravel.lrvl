  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <!-- encabezado modal -->
        <div class="card-header card-header-primary">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>
          </button>
          <h4 class="card-title">{{ $title }}</h4>
          <p class="card-category">Complete su perfil</p>
        </div>
        <!-- END Encabezado modal -->
        <!-- Cuerpo Modal -->
        <div class="modal-body">
          <form id="form-modificar" action="{{ route('usuarios.editUsuario') }}" method="post" role="form">
            {{-- {{ csrf_field() }} --}}  {{-- <-proteccion contra ataques XSS --}}
            @csrf {{-- <-proteccion contra ataques XSS --}}
            @forelse ($users as $user)
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">First Name</label>
                    <input type="text" name="firstName" class="form-control" value="{{ $user->firstName }}">
                    <input type="hidden" name="id" value="{{ $user->id }}">
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Last Name</label>
                    <input type="text" name="lastName" class="form-control" value="{{ $user->lastName }}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group">
                    <label class="bmd-label-floating">Username</label>
                    <input type="text" name="username" class="form-control text-center" value="{{ $user->username }}">
                  </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3">
                  <div class="form-group">
                    <label class="bmd-label-floating">Password</label>
                    <input type="password" name="password" class="form-control text-center">
                  </div>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-5">
                  <div class="form-group">
                    <label class="bmd-label-floating">Email address</label>
                    <input type="email" name="email" class="form-control text-center" value="{{ $user->email }}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12  col-sm-12 col-xs-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Address</label>
                    <input type="text" name="address" class="form-control" value="{{ $user->address }}">
                  </div>
                </div>
                <div class="col-md-7  col-sm-7 col-xs-7">
                  <div class="form-group">
                    <label class="bmd-label-floating">Website:</label>
                    <input type="text" name="website" class="form-control" value="{{ $user->website }}">
                  </div>
                </div>
                <div class="col-md-5  col-sm-5 col-xs-5">
                  <div class="form-group">
                    <label class="bmd-label-floating">Teléfono:</label>
                    <input type="text" name="phone" class="form-control" value="{{ $user->phone }}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-5 col-xs-5">
                  <div class="form-group">
                    <label class="bmd-label-floating">Profesión</label>
                    <select name="profession_id" class="form-control">
                      <option value="{{ $user->profession_id }}">{{ $user->profession['title'] }}</option>
                      @foreach ($profes as $prof)
                        <option value="{{ $prof->id }}">{{ $prof->title }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-6 col-sm-4 col-xs-4">
                  <div class="form-group">
                    <label class="bmd-label-floating">Habilidades</label>
                    <select name="skill_id" class="form-control">
                      <option value="{{ $user->skill_id }}">{{ $user->skill->skill }}</option>
                      @foreach ($skills as $skill)
                        <option value="{{ $skill->id }}">{{ $skill->skill }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-4 col-sm-3 col-xs-3">
                  <div class="form-group">
                    <label class="bmd-label-floating">Es Administrador?</label>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input name="isAdmin" class="form-check-input" type="radio" <?php if($user->isAdmin == 1){ echo 'checked="checked"';} ?> value="1" />Si
                        <span class="form-check-sign">
                          <span class="check"></span>
                        </span>
                      </label>
                      <label class="form-check-label">
                        <input name="isAdmin" class="form-check-input" type="radio" <?php if($user->isAdmin == 0){ echo 'checked="checked"';} ?> value="0" />No
                        <span class="form-check-sign">
                          <span class="check"></span>
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              {{-- <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <label>Acerca de Mi</label>
                    <div class="form-group">
                      <label class="bmd-label-floating"></label>
                      <textarea name="aboutme" class="form-control" rows="2" onkeydown="if(this.value.length >= 80){ alert('Has superado el tama&ntilde;o m&aacute;ximo permitido en la dirección'); return false; }" onchange="javascript:this.value=this.value.toUpperCase();"></textarea>
                    </div>
                  </div>
                </div>
              </div> --}}
              <div class="modal-footer col-md-12">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" name="Modificar" class="btn btn-primary" value="Modificar" title="Modificar Datos" >Actualizar Perfil</button>
              </div>
              <div class="clearfix"></div>
            @empty
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="bmd-label-floating">No existe este Registro.</label>
                  </div>
                </div>
              </div>
            @endforelse
          </form>
        </div>
        <!-- END Cuerpo Modal -->
      </div>
    </div>
  </div>