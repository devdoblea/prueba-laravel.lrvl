  <div class="row">

    <div class="col-lg-12 col-md-12">
      <div class="card">
        <div class="card-header card-header-info">
          <h4 class="card-title">{{ $title }}</h4>
          <p class="card-category">Nuevos empleados a la fecha</p>
        </div>
        <div class="card-body table-responsive">
          <table class="table table-hover">
            <thead class="text-warning">
              <th class="text-center">ID</th>
              <th class="text-left"> Name</th>
              <th class="text-center">Correo</th>
              <th class="text-center">Cargo</th>
              <th class="text-center" colspan="2">Accion</th>
            </thead>
            <tbody>
                @forelse ($users as $user)
                  <tr>
                    <td class="text-center">{{ $user->id }}</td>
                    <td class="text-left">  {{ $user->name }}</td>
                    <td class="text-center">{{ $user->email }}</td>
                    <td class="text-center">{{ $user->profession->title }}</td>
                    <td class="td-actions text-center">
                      <button type="button" rel="tooltip" title="Edita Usuario" class="btn btn-primary btn-link btn-sm">
                        <i class="material-icons">edit</i>
                      </button>
                    </td>
                    <td class="td-actions text-center">
                      <button type="button" rel="tooltip" title="Remover Usuario" class="btn btn-danger btn-link btn-sm">
                        <i class="material-icons">close</i>
                      </button>
                    </td>
                  </tr>
                @empty
                  <tr>
                    <td colspan="5">No hay Usuarios Registrados.</td>
                  </tr>
                @endforelse
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
