  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <!-- encabezado modal -->
        <div class="card-header card-header-danger">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span>
          </button>
          <h4 class="card-title">{{ $title }}</h4>
          <p class="card-category">Responda la pregunta</p>
        </div>
        <!-- END Encabezado modal -->
        <!-- Cuerpo Modal -->
        <div class="modal-body">
          <form id="form-borrar" action="{{ route('usuarios.delUsuario') }}" method="post" role="form">
            {{-- {{ csrf_field() }} --}}  {{-- <-proteccion contra ataques XSS --}}
            @csrf {{-- <-proteccion contra ataques XSS --}}
            @foreach ($users as $user)
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group text-center">
                    <label class="bmd-label-floating">Nombre de Usuario</label>
                    <h2>{{ $user->firstName.' '.$user->lastName }}</h2>
                    <input type="hidden" name="id" value="{{ $user->id }}">
                  </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group text-center">
                    <label class="bmd-label-floating">Borrar este Usuario?</label>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input name="borrar_id" class="form-check-input" type="radio" value="Si" />Si
                        <span class="form-check-sign">
                          <span class="check"></span>
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
              <div class="modal-footer">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group text-center">
                    <button type="submit" name="Borrar" class="btn btn-danger" value="Borrar" title="Borrar Usuario" >Borrar este Usuario</button>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
          </form>
        </div>
        <!-- END Cuerpo Modal -->
      </div>
    </div>
  </div>