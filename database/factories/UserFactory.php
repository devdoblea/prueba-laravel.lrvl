<?php

use Faker\Generator as Faker;
use App\Http\Models\Profession as Profesion; // llamo para que funcionen los metodos (funciones) del Modelo Profession
use App\Http\Models\Skill_Model as Skill; // llamo para que funcionen los metodos (funciones) del Modelo Skill_Model

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Http\Models\User::class, function (Faker $faker) {
    return [
    	'username'   => $faker->username,
        'firstName'  => $faker->firstName,
        'LastName'   => $faker->lastName,
        'email'      => $faker->unique()->safeEmail,
        'address'    => $faker->address,
        'phone'      => $faker->phoneNumber,
        'isAdmin'    => false,
        'profession_id' => Profesion::orderByRaw('RAND()')->take(1)->value('id'),// traigo valores random
        'skill_id' => Skill::orderByRaw('RAND()')->take(1)->value('id'),// traigo valores 
        'email_verified_at' => now(),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});
