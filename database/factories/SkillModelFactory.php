<?php

use Faker\Generator as Faker;

$factory->define(App\Http\Models\Skill_Model::class, function (Faker $faker) {
    return [
        'skill' => $faker->sentence(1),
    ];
});
