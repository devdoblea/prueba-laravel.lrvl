<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
				//dd(ProfessionSeeder::class);
        // $this->call(UsersTableSeeder::class);
				$this->truncateTables([
					'professions',
					'users',
					'skill'
				]);

				$this->call(profession_seeder::class);
				$this->call(skill_seeder::class);
				$this->call(user_seeder::class);
    }

	public function truncateTables(array $tables)
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');

		foreach ($tables as $table) {
			DB::table($table)->truncate();
		}

		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}
}
