<?php

//use App\Http\Models\Profession as Profesion; // llamo para que funcionen los metodos (funciones) del Modelo Profession
use App\Http\Models\Profession;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class profession_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    //public function run()
    //{
        //DB::insert('INSERT INTO professions (title) VALUES ("Desarrollador back-End")');
        //DB::insert('INSERT INTO professions (title) VALUES (:title)',['title' => "Desarrollador Back-End"]);

        // DB::table('professions')->insert([
        //  'title' => 'Desarrollador Back-End',
        // ]);

        // DB::table('professions')->insert([
        //  'title' => 'Diseñador Gráfico',
        // ]);

        // DB::table('professions')->insert([
        //  'title' => 'Desarrollador Front-End',
        // ]);

        // UTILIZANDO ELOCUENT, EL MANEJADOR DE BASE DE DATOS DE LARAVEL
        // App\Http\Models\Profession::create([
        //     'title' => 'Diseñador grafico',
        // ]);
        // App\Http\Models\Profession::create([
        //     'title' => 'Desarrollador Back-End',
        // ]);
        // App\Http\Models\Profession::create([
        //     'title' => 'Desarrollador Front-End',
        // ]);
    //}
    public function run()
    {
        //USANDO LOS NAMESPACE Y LLAMANDO A LA FUNCION CREATE CON COMANDOS PARA MANEJAR ELOCUENT->BASE DE DATOS DE LARAVEL
       Profession::create([
            'title' => 'Diseñador grafico',
        ]);
       Profession::create([
            'title' => 'Desarrollador Back-End',
        ]);
       Profession::create([
            'title' => 'Desarrollador Front-End',
        ]);

       // cargar "magicamente" mas profesiones con el helper "FACTORY"
       //factory(Profession::class, 17)->create();
       factory(Profession::class)->times(17)->create();


    }
}
