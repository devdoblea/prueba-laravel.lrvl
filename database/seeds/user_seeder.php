<?php

use App\Http\Models\User as User; // llamo para que funcionen los metodos (funciones) del Modelo User
use App\Http\Models\Profession as Profesion; // llamo para que funcionen los metodos (funciones) del Modelo Profession
use App\Http\Models\Skill_Model as Skill; // llamo para que funcionen los metodos (funciones) del Modelo Skill_Model
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB; // este facade solo sirve cuando se usa "DB::"

class user_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //$professions = DB::select('SELECT id FROM professions WHERE title = "Desarrollador Back-End"');
        //$professions = DB::select('SELECT id FROM professions WHERE title = ?',['Desarrollador Back-End']); // traigo un registro con un nombre especifico 
        //$professions = DB::select('SELECT id FROM professions WHERE title = ? LIMIT 0,1',['Desarrollador Back-End']); // traigo solo un registro
        
        //$professions = DB::select('SELECT id FROM professions');// traigo un valor consultandolo
        
        //$profession = Profesion::where('title', 'Desarrollador Back-End')->value('id');// traigo un valor consultandolo a travez de sentencias ELOCUENT

        //dd($professions)->first()->id; // professions[0]

        //$prof = DB::table('professions')->select('id')->where(['title' =>'Diseñador Gráfico'])->first(); (No sirvio en esta version, en la version 5.5 si funciona)

        // asi se utiliza el manejador de datos nativo de laravel
        // DB::table('users')->insert([
        // 	'name'     => 'Rossangel Antunez',
        // 	'email'    => 'rosa@admin.com',
        // 	'password' => bcrypt('laravel'),
        //     'profession_id' => $professions[0]->id,
        // ]);
        // DB::table('users')->insert([
        // 	'name'     => 'Mariangel Antunez',
        // 	'email'    => 'gigel@admin.com',
        // 	'password' => bcrypt('laravel'),
        //     'profession_id' => $professions[1]->id,
        // ]);
        // DB::table('users')->insert([
        // 	'name'     => 'Moises Antunez',
        // 	'email'    => 'moises@admin.com',
        // 	'password' => bcrypt('laravel'),
        //     'profession_id' => $professions[2]->id,
        // ]);
        // DB::table('users')->insert([
        // 	'name'     => 'Ana Monserrat Antunez',
        // 	'email'    => 'anam@admin.com',
        // 	'password' => bcrypt('laravel'),
        //     'profession_id' => $professions[2]->id,
        // ]);

        // Asi usaremos el Manejador de base de datos ELOCUENT dentro de Laravel
        User::create([
            'username' => 'rossaross',
            'firstName'=> 'Rossangel',
            'LastName' => 'Antunez',
            'email'    => 'rosa@admin.com',
            'address'  => 'Calle Principal',
            'phone'    => '424-861-908-12',
            'password' => bcrypt('laravel'),
            'isAdmin'  => true,
            'profession_id' => 1,
            'skill_id' => Skill::orderByRaw('RAND()')->take(1)->value('id'),// traigo valores 
        ]);
        User::create([
            'username' => 'soygigel',
            'firstName'=> 'Mariangel',
            'LastName' => 'Antunez',
            'email'    => 'gigel@admin.com',
            'address'  => 'Calle Principal',
            'phone'    => '424-861-908-12',
            'password' => bcrypt('laravel'),
            'isAdmin'  => false,
            'profession_id' => Profesion::orderByRaw('RAND()')->take(1)->value('id'),// traigo valores random
            'skill_id' => Skill::orderByRaw('RAND()')->take(1)->value('id'),// traigo valores 
        ]);
        User::create([
            'username' => 'moemoe',
            'firstName'=> 'Moises',
            'LastName' => 'Antunez',
            'email'    => 'moises@admin.com',
            'address'  => 'Calle Principal',
            'phone'    => '424-861-908-12',
            'password' => bcrypt('laravel'),
            'isAdmin'  => false,
            'profession_id' => Profesion::orderByRaw('RAND()')->take(1)->value('id'),// traigo valores random
            'skill_id' => Skill::orderByRaw('RAND()')->take(1)->value('id'),// traigo valores 
        ]);
        User::create([
            'username' => 'anamontse',
            'firstName'=> 'Ana Monserrat',
            'LastName' => 'Antunez',
            'email'    => 'anam@admin.com',
            'address'  => 'Calle Principal',
            'phone'    => '424-861-908-12',
            'password' => bcrypt('laravel'),
            'isAdmin'  => false,
            'profession_id' => Profesion::orderByRaw('RAND()')->take(1)->value('id'),// traigo valores random
            'skill_id' => Skill::orderByRaw('RAND()')->take(1)->value('id'),// traigo valores 
        ]);

        // cargar "magicamente" mas profesiones con el helper "FACTORY"
       //factory(Profession::class, 17)->create();
       factory(User::class)->times(26)->create();

    }
}
