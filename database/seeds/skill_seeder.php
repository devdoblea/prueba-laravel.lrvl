<?php

//use App\Http\Models\Profession as Profesion; // llamo para que funcionen los metodos (funciones) del Modelo Profession
use App\Http\Models\Skill_Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class skill_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //USANDO LOS NAMESPACE Y LLAMANDO A LA FUNCION CREATE CON COMANDOS PARA MANEJAR ELOCUENT->BASE DE DATOS DE LARAVEL
       Skill_Model::create([
            'skill' => 'Pensador',
        ]);

       factory(Skill_Model::class)->times(19)->create();


    }
}
