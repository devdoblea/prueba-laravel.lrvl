<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id'); // INTEGER UNSIGNED
            $table->string('username',20);// VARCHAR
            $table->string('firstName',30);// VARCHAR
            $table->string('lastName',30);// VARCHAR
            $table->string('email')->unique();// VARCHAR
            $table->string('address',100);
            $table->string('phone',16);
            $table->string('password');
            $table->boolean('isAdmin');
            $table->string('website')->nullable();
            $table->timestamp('email_verified_at')->nullable(); // HORA  Y NULABLE
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
