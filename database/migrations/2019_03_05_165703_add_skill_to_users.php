<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSkillToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('skill_id')->after('profession_id')->nullable();
            $table->foreign('skill_id')->references('id')->on('skill');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS = 0'); // para que pueda borrar los foreing_key que se consigan mas adelante

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('skill_id');
            $table->dropColumn('skill_id');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS = 1'); // reactivo la validacion de foreing_key para que se mantenga estable la base d edatos
    }
}
