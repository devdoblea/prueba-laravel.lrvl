<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkillModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skill', function (Blueprint $table) {
            $table->increments('id');
            $table->string('skill',60);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0'); // para que pueda borrar los foreing_key que se consigan mas adelante

        Schema::dropIfExists('skill');

        DB::statement('SET FOREIGN_KEY_CHECKS = 1'); // reactivo la validacion de foreing_key para que se mantenga estable la base d edatos
    }
}
