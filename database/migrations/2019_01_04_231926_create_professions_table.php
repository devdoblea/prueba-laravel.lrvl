<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',100)->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0'); // para que pueda borrar los foreing_key que se consigan mas adelante
        
        Schema::dropIfExists('professions');

        DB::statement('SET FOREIGN_KEY_CHECKS = 1'); // reactivo la validacion de foreing_key para que se mantenga estable la base d edatos
    }
}
