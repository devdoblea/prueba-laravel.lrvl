-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- DROP TABLE "migrations" -------------------------------------
DROP TABLE IF EXISTS `migrations` CASCADE;
-- -------------------------------------------------------------


-- CREATE TABLE "migrations" -----------------------------------
CREATE TABLE `migrations` ( 
	`id` Int( 10 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`migration` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`batch` Int( 11 ) NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 7;
-- -------------------------------------------------------------


-- DROP TABLE "password_resets" --------------------------------
DROP TABLE IF EXISTS `password_resets` CASCADE;
-- -------------------------------------------------------------


-- CREATE TABLE "password_resets" ------------------------------
CREATE TABLE `password_resets` ( 
	`email` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`token` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`created_at` Timestamp NULL )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------


-- DROP TABLE "professions" ------------------------------------
DROP TABLE IF EXISTS `professions` CASCADE;
-- -------------------------------------------------------------


-- CREATE TABLE "professions" ----------------------------------
CREATE TABLE `professions` ( 
	`id` Int( 10 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`title` VarChar( 100 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `professions_title_unique` UNIQUE( `title` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 21;
-- -------------------------------------------------------------


-- DROP TABLE "skill" ------------------------------------------
DROP TABLE IF EXISTS `skill` CASCADE;
-- -------------------------------------------------------------


-- CREATE TABLE "skill" ----------------------------------------
CREATE TABLE `skill` ( 
	`id` Int( 10 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`skill` VarChar( 60 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 21;
-- -------------------------------------------------------------


-- DROP TABLE "users" ------------------------------------------
DROP TABLE IF EXISTS `users` CASCADE;
-- -------------------------------------------------------------


-- CREATE TABLE "users" ----------------------------------------
CREATE TABLE `users` ( 
	`id` Int( 10 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`username` VarChar( 20 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`firstName` VarChar( 30 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`lastName` VarChar( 30 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`profession_id` Int( 10 ) UNSIGNED NULL,
	`skill_id` Int( 10 ) UNSIGNED NULL,
	`email` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`address` VarChar( 100 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`phone` VarChar( 16 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`password` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`isAdmin` TinyInt( 1 ) NOT NULL,
	`website` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`email_verified_at` Timestamp NULL,
	`remember_token` VarChar( 100 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `users_email_unique` UNIQUE( `email` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 33;
-- -------------------------------------------------------------


-- Dump data of "migrations" -------------------------------
REPLACE INTO `migrations`(`id`,`migration`,`batch`) VALUES 
( '1', '2014_10_12_000000_create_users_table', '1' ),
( '2', '2014_10_12_100000_create_password_resets_table', '1' ),
( '3', '2019_01_04_231926_create_professions_table', '1' ),
( '4', '2019_01_04_234606_add_professions_id_to_users', '1' ),
( '5', '2019_03_02_013924_create_skill__models_table', '1' ),
( '6', '2019_03_05_165703_add_skill_to_users', '1' );
-- ---------------------------------------------------------


-- Dump data of "password_resets" --------------------------
-- ---------------------------------------------------------


-- Dump data of "professions" ------------------------------
REPLACE INTO `professions`(`id`,`title`,`created_at`,`updated_at`) VALUES 
( '1', 'Diseñador grafico', '2019-03-22 04:43:27', '2019-03-22 04:43:27' ),
( '2', 'Desarrollador Back-End', '2019-03-22 04:43:27', '2019-03-22 04:43:27' ),
( '3', 'Desarrollador Front-End', '2019-03-22 04:43:27', '2019-03-22 04:43:27' ),
( '4', 'Et rem in.', '2019-03-22 04:43:27', '2019-03-22 04:43:27' ),
( '5', 'Porro repellat.', '2019-03-22 04:43:27', '2019-03-22 04:43:27' ),
( '6', 'Culpa illum tempore.', '2019-03-22 04:43:28', '2019-03-22 04:43:28' ),
( '7', 'Veritatis minus aut.', '2019-03-22 04:43:28', '2019-03-22 04:43:28' ),
( '8', 'Modi eum numquam.', '2019-03-22 04:43:28', '2019-03-22 04:43:28' ),
( '9', 'Doloribus non libero.', '2019-03-22 04:43:28', '2019-03-22 04:43:28' ),
( '10', 'Recusandae debitis.', '2019-03-22 04:43:28', '2019-03-22 04:43:28' ),
( '11', 'Corporis aliquam voluptas.', '2019-03-22 04:43:28', '2019-03-22 04:43:28' ),
( '12', 'Ea laudantium.', '2019-03-22 04:43:28', '2019-03-22 04:43:28' ),
( '13', 'Aliquid consequatur velit.', '2019-03-22 04:43:28', '2019-03-22 04:43:28' ),
( '14', 'Et impedit cumque.', '2019-03-22 04:43:28', '2019-03-22 04:43:28' ),
( '15', 'Pariatur in earum.', '2019-03-22 04:43:28', '2019-03-22 04:43:28' ),
( '16', 'Quae delectus doloremque.', '2019-03-22 04:43:28', '2019-03-22 04:43:28' ),
( '17', 'Cupiditate commodi.', '2019-03-22 04:43:28', '2019-03-22 04:43:28' ),
( '18', 'Consequatur rerum.', '2019-03-22 04:43:28', '2019-03-22 04:43:28' ),
( '19', 'Et quia.', '2019-03-22 04:43:28', '2019-03-22 04:43:28' ),
( '20', 'Autem enim.', '2019-03-22 04:43:28', '2019-03-22 04:43:28' );
-- ---------------------------------------------------------


-- Dump data of "skill" ------------------------------------
REPLACE INTO `skill`(`id`,`skill`,`created_at`,`updated_at`) VALUES 
( '1', 'Pensador', '2019-03-22 04:43:28', '2019-03-22 04:43:28' ),
( '2', 'Necessitatibus provident.', '2019-03-22 04:43:29', '2019-03-22 04:43:29' ),
( '3', 'Minus.', '2019-03-22 04:43:29', '2019-03-22 04:43:29' ),
( '4', 'Nemo.', '2019-03-22 04:43:29', '2019-03-22 04:43:29' ),
( '5', 'Soluta.', '2019-03-22 04:43:29', '2019-03-22 04:43:29' ),
( '6', 'Hic dicta.', '2019-03-22 04:43:29', '2019-03-22 04:43:29' ),
( '7', 'Placeat veniam.', '2019-03-22 04:43:29', '2019-03-22 04:43:29' ),
( '8', 'Qui deleniti.', '2019-03-22 04:43:29', '2019-03-22 04:43:29' ),
( '9', 'Eum fugit.', '2019-03-22 04:43:29', '2019-03-22 04:43:29' ),
( '10', 'Voluptatem sed.', '2019-03-22 04:43:29', '2019-03-22 04:43:29' ),
( '11', 'Dolor.', '2019-03-22 04:43:29', '2019-03-22 04:43:29' ),
( '12', 'Et.', '2019-03-22 04:43:29', '2019-03-22 04:43:29' ),
( '13', 'Debitis.', '2019-03-22 04:43:29', '2019-03-22 04:43:29' ),
( '14', 'Aut at.', '2019-03-22 04:43:29', '2019-03-22 04:43:29' ),
( '15', 'Alias alias.', '2019-03-22 04:43:29', '2019-03-22 04:43:29' ),
( '16', 'Nam soluta.', '2019-03-22 04:43:29', '2019-03-22 04:43:29' ),
( '17', 'Est.', '2019-03-22 04:43:29', '2019-03-22 04:43:29' ),
( '18', 'Qui.', '2019-03-22 04:43:29', '2019-03-22 04:43:29' ),
( '19', 'Atque aperiam.', '2019-03-22 04:43:29', '2019-03-22 04:43:29' ),
( '20', 'Ut.', '2019-03-22 04:43:29', '2019-03-22 04:43:29' );
-- ---------------------------------------------------------


-- Dump data of "users" ------------------------------------
REPLACE INTO `users`(`id`,`username`,`firstName`,`lastName`,`profession_id`,`skill_id`,`email`,`address`,`phone`,`password`,`isAdmin`,`website`,`email_verified_at`,`remember_token`,`created_at`,`updated_at`) VALUES 
( '1', 'rossaross', 'Rossangel', 'Antunez', '1', '1', 'rosa@admin.com', 'Calle Principal', '424-861-908-12', '$2y$10$X2IWlsn9Z4VaWlFpQHA4jOsAYdoJo2lSjant50184uQvfOxJOb0ja', '1', NULL, NULL, NULL, '2019-03-22 04:43:30', '2019-03-22 04:43:30' ),
( '2', 'soygigel', 'Mariangel', 'Antunez', '18', '13', 'gigel@admin.com', 'Calle Principal', '424-861-908-12', '$2y$10$0ExQCsgWJbgeWhfr59YIHefpWT1eqdf.YLcz9S8u.kqIbeQSAgqDe', '0', NULL, NULL, NULL, '2019-03-22 04:43:30', '2019-03-22 04:43:30' ),
( '3', 'moemoe', 'Moises', 'Antunez', '19', '13', 'moises@admin.com', 'Calle Principal', '424-861-908-12', '$2y$10$Q2J8N7R/StbeohSsMfRo6.kwp9sw4cM57mB9EVivJAOwujB/erQhS', '0', NULL, NULL, NULL, '2019-03-22 04:43:30', '2019-03-22 04:43:30' ),
( '4', 'anamontse', 'Ana Monserrat', 'Antunez', '13', '7', 'anam@admin.com', 'Calle Principal', '424-861-908-12', '$2y$10$Sf1tpiOiezjpGuEupsA.eu3lb66lBqzV2GTB9v474LN8.lzU6hwP2', '0', NULL, NULL, NULL, '2019-03-22 04:43:30', '2019-03-22 04:43:30' ),
( '5', 'jmunoz', 'Ariadna', 'Gutiérrez', '9', '10', 'meza.luis@example.net', 'Plaza Olivia, 251, 2º A, 42696, As Herrera', '661-889328', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', '3SsnU4wvyv', '2019-03-22 04:43:30', '2019-03-22 04:43:30' ),
( '6', 'espino.patricia', 'Miguel Ángel', 'Reyes', '19', '7', 'bruno.millan@example.com', 'Ruela Jimena, 519, 58º D, 40967, De la Torre del Mirador', '+34 900-936584', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', '8VUCnvxSab', '2019-03-22 04:43:31', '2019-03-22 04:43:31' ),
( '7', 'miguel15', 'Miriam', 'Murillo', '20', '8', 'fmontanez@example.org', 'Plaza Marco, 21, 4º E, 92673, Rosado de las Torres', '+34 634838649', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', 'A6fXncS4eG', '2019-03-22 04:43:31', '2019-03-22 04:43:31' ),
( '8', 'isabel.delagarza', 'Olivia', 'Betancourt', '10', '3', 'cordero.mireia@example.com', 'Calle Zamora, 2, 0º 2º, 56901, Segovia del Mirador', '+34 950 66 5890', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', 'EB3WOE9Edp', '2019-03-22 04:43:31', '2019-03-22 04:43:31' ),
( '9', 'omarin', 'Mario', 'Sandoval', '3', '6', 'batista.david@example.net', 'Avinguda Barrios, 2, 76º E, 87601, Os Cadena', '+34 630949886', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', 'cm4AkFgMy4', '2019-03-22 04:43:31', '2019-03-22 04:43:31' ),
( '10', 'blazquez.diego', 'César', 'Pabón', '14', '16', 'guerra.diego@example.net', 'Plaza Adam, 449, 4º D, 29780, Los Macías', '671628274', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', 'fjrXESPb7E', '2019-03-22 04:43:31', '2019-03-22 04:43:31' ),
( '11', 'puente.marco', 'Daniel', 'Oliva', '8', '9', 'ygastelum@example.org', 'Avenida Ander, 257, 65º B, 40203, Vall Vila', '988-06-4639', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', '8tA71wnftx', '2019-03-22 04:43:31', '2019-03-22 04:43:31' ),
( '12', 'hernandes.erika', 'Aitana', 'Olvera', '15', '15', 'fatima66@example.com', 'Plaza Meza, 7, 5º D, 35110, La Sanz del Vallès', '+34 939358032', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', 'NAtt6YbR6O', '2019-03-22 04:43:31', '2019-03-22 04:43:31' ),
( '13', 'pedro26', 'Pau', 'Carranza', '7', '6', 'cpaez@example.net', 'Avinguda Peralta, 54, 1º, 19026, As Rosas de Ulla', '+34 619 40 6082', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', 'PUx53MEUSO', '2019-03-22 04:43:31', '2019-03-22 04:43:31' ),
( '14', 'nmarin', 'Manuela', 'Valenzuela', '11', '20', 'aleix.vazquez@example.com', 'Camino Manzanares, 4, 49º D, 52072, Villa Castaño', '+34 951 150910', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', 'AYLqdaWLMs', '2019-03-22 04:43:31', '2019-03-22 04:43:31' ),
( '15', 'jan46', 'Nil', 'Varela', '12', '3', 'enrique68@example.org', 'Travessera Arreola, 6, 47º E, 61339, La Almaráz de Arriba', '+34 620 781659', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', 'MQQfnFx3XB', '2019-03-22 04:43:31', '2019-03-22 04:43:31' ),
( '16', 'iperez', 'Ander', 'Gámez', '19', '11', 'marta69@example.org', 'Carrer Más, 5, 86º A, 24032, As Villalba del Pozo', '+34 985-948612', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', 'ywyU8WAyuA', '2019-03-22 04:43:31', '2019-03-22 04:43:31' ),
( '17', 'ledesma.carmen', 'Leire', 'Chavarría', '1', '11', 'lllamas@example.net', 'Camiño Cisneros, 54, 8º B, 78435, As Tovar de la Sierra', '+34 676-431799', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', '1VGqr7Vx0M', '2019-03-22 04:43:31', '2019-03-22 04:43:31' ),
( '18', 'carlota.ruiz', 'Óscar', 'Regalado', '16', '7', 'marquez.antonio@example.com', 'Avenida Armenta, 632, 0º, 36504, Villa Segura Baja', '+34 602030576', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', 'NK4JqvMEi0', '2019-03-22 04:43:31', '2019-03-22 04:43:31' ),
( '19', 'aitana.ibarra', 'Ignacio', 'Longoria', '11', '2', 'zsalas@example.org', 'Ronda Ariadna, 5, 42º E, 12713, Mata del Barco', '+34 629 11 0968', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', '2g2qp9o2Mk', '2019-03-22 04:43:31', '2019-03-22 04:43:31' ),
( '20', 'marc28', 'Laia', 'Saiz', '20', '3', 'ybarra.sara@example.net', 'Ronda Unai, 85, Ático 2º, 55788, Cobo de la Sierra', '+34 917-70-8238', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', 'jIYwbpX8Ju', '2019-03-22 04:43:31', '2019-03-22 04:43:31' ),
( '21', 'jguerra', 'Miguel Ángel', 'Aguayo', '10', '6', 'gaitan.ane@example.net', 'Passeig Eric, 0, 5º C, 01463, Nieto del Barco', '+34 998 875167', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', 'GOH7orxi0m', '2019-03-22 04:43:31', '2019-03-22 04:43:31' ),
( '22', 'guillem.ontiveros', 'Sara', 'Casado', '12', '11', 'mario50@example.net', 'Ronda Alexandra, 6, 4º B, 36032, A Morales de Lemos', '989 223201', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', '3E60RT84tU', '2019-03-22 04:43:32', '2019-03-22 04:43:32' ),
( '23', 'rojas.roberto', 'Jordi', 'Escamilla', '7', '10', 'luna.montano@example.org', 'Camiño Solís, 11, Ático 5º, 43584, L\' Cerda Baja', '+34 946-53-4566', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', '9kNsQGP9bm', '2019-03-22 04:43:32', '2019-03-22 04:43:32' ),
( '24', 'pagan.patricia', 'Jon', 'Aguilera', '9', '19', 'heredia.nora@example.net', 'Ronda Nil, 09, 49º A, 36907, As Adame', '+34 907-186917', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', '4Md73LBBJU', '2019-03-22 04:43:32', '2019-03-22 04:43:32' ),
( '25', 'elena.cerda', 'Ona', 'Velasco', '1', '4', 'carmen.molina@example.com', 'Carrer Carla, 555, 30º E, 95240, Los Lovato', '637 003222', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', 'AkV4ebMZ2A', '2019-03-22 04:43:32', '2019-03-22 04:43:32' ),
( '26', 'isaac96', 'Saúl', 'Cerda', '10', '19', 'rosa.mara@example.com', 'Passeig Montalvo, 6, 97º 1º, 86682, El Lara del Vallès', '613-71-2862', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', 'J49lBKRBoV', '2019-03-22 04:43:32', '2019-03-22 04:43:32' ),
( '27', 'julia.leon', 'Leire', 'Treviño', '9', '15', 'wtello@example.org', 'Passeig Dario, 058, 20º B, 48841, Negrete del Puerto', '602229110', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', 'ASrykfkwI1', '2019-03-22 04:43:32', '2019-03-22 04:43:32' ),
( '28', 'celia22', 'Leyre', 'Perea', '2', '11', 'ruiz.pablo@example.net', 'Calle Rivero, 27, 3º E, 04651, Las Cortez', '625188522', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', 'ZkXTs9ZqKJ', '2019-03-22 04:43:32', '2019-03-22 04:43:32' ),
( '29', 'cbarrios', 'Raúl', 'Niño', '3', '11', 'zaleman@example.com', 'Praza Matos, 8, 90º B, 99277, El Muro del Bages', '605 578700', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', 'ZEqaFHJdJ7', '2019-03-22 04:43:32', '2019-03-22 04:43:32' ),
( '30', 'iruiz', 'Eduardo', 'Moya', '6', '10', 'noa.regalado@example.com', 'Plaza Gerard, 54, Bajo 4º, 95350, Tórrez del Mirador', '929 289897', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '0', NULL, '2019-03-22 04:43:30', 'KMR23wSyS9', '2019-03-22 04:43:32', '2019-03-22 04:43:32' ),
( '31', 'doblea71', 'Angel', 'Antunez', '2', '2', 'doblea71@gmail.com', 'calle principal', '424-123-23-23', '$2y$10$1xP/6IcG865qr6w4U0y.tOVobgPMoOmaDHG8RtkaatrUEowxMD9We', '1', 'elcuartodeangel.com', NULL, NULL, '2019-03-22 04:44:23', '2019-03-22 04:44:23' ),
( '32', 'doblea71', 'Angel Emiro', 'Antunez', '1', '1', 'doblea71@laravel.com', 'Calle Principal la victoria', '424-987-32-12', '$2y$04$qqnBlu1FdiS2CKQj8/oaauATElKOV5A3PdsXhTAHjuAgCMiZwv0B.', '1', 'elcuartodeangel.wordpress.com', NULL, NULL, '2019-03-22 13:10:07', '2019-03-25 15:15:59' );
-- ---------------------------------------------------------


-- CREATE INDEX "password_resets_email_index" ------------------
CREATE INDEX `password_resets_email_index` USING BTREE ON `password_resets`( `email` );
-- -------------------------------------------------------------


-- CREATE INDEX "users_profession_id_foreign" ------------------
CREATE INDEX `users_profession_id_foreign` USING BTREE ON `users`( `profession_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "users_skill_id_foreign" -----------------------
CREATE INDEX `users_skill_id_foreign` USING BTREE ON `users`( `skill_id` );
-- -------------------------------------------------------------


-- CREATE LINK "users_profession_id_foreign" -------------------
ALTER TABLE `users`
	ADD CONSTRAINT `users_profession_id_foreign` FOREIGN KEY ( `profession_id` )
	REFERENCES `professions`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


-- CREATE LINK "users_skill_id_foreign" ------------------------
ALTER TABLE `users`
	ADD CONSTRAINT `users_skill_id_foreign` FOREIGN KEY ( `skill_id` )
	REFERENCES `skill`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


