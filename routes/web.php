<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/********************************************************************
/* Ruta hacia la pagina principal pero ya usando Material-dashboard
*/
//Route::get('/', 'MainController@index')->name('main');

/**********************************************************************
* Rutas de pueba para el inicio del curso de duilio palacios laravel 5.5
// Route::get('/usuarios', function () {
//     return 'Usuarios';
// });

// Ruta hacia la tabla de usuarios
//Route::get('/usuarios', 'UserController@index');
//Route::get('/usuarios',['as' => 'usuarios', 'uses' => 'UserController@index']);
/*Route::get('/usuarios', 'UserController@index', function()
{
	return View::make('front-end.pruebaUsuarios');
}); */


// usuarios/nuevo =! usuarios/[0-9]
// Route::get('/usuarios/{id}', function ($id) {
//     return "Mostrando detalles del usuario Nro: {$id}";
// })->where('id', '[0-9]+');

//Route::get('/usuarios/{id}', 'UserController@show')->where('id', '[0-9]+');


// Route::get('/usuarios/nuevo', function () {
//     return 'Crear nuevo usuario';
// });
// Ruta para mostrar la ventana modal del formulario AGREGAR USUARIO
//Route::get('/usuarios/nuevo','UserController@create')->name('usuarios.nuevo');

//Para el registro de usuarios
//Route::get('/usuarios/nuevo','UserController@create');

// Route::get('/saludo/{name}/{nickname?}', function ($name, $nickname = null) {
// 		$name = ucfirst($name);
// 		if ($nickname) {
// 			return "Bienvenido {$name}, tu apodo es {$nickname}";
// 		} else {
// 			return "Bienvenido {$name}";
// 		}
// });

//Route::get('/saludo/{name}/{nickname?}','WelcomeUserController@index');

//Route::get('/saludo/{name}/{nickname?}','WelcomeUserController'); // usando una funcion unica en el controlador

// Route::get('/usuarios/detalles', function () {
//     return view('users');
// });

/**************************************************************
 * Rutas para usar la plantilla Material Design 5
*/
//Ruta para la pantalla inicial
Route::get('/', 'MainController@index')
			->name('main');

// Ruta para mostrar la "Lista de Usuarios"
Route::get('/usuarios', 'UserController@index')
			->name('usuarios');

/*****************************************************************************
* RUTAS PARA MOSTRAR FORMULARIOS O PAGINAS*/

// Ruta para mostrar la ventana modal del formulario AGREGAR USUARIO
Route::get('/usuarios/modalregusuario','UserController@modalRegUsuario')
 			->name('usuarios.modalregusuario');

/* Ruta para mostrar la ventana modal del formulario EDITAR USUARIO
 * Se debe usar el metodo "POST" porque se necesita enviar el ID del
 * usuario a editar. Se usa AJAX para enviar y recibir datos */
Route::post('/usuarios/modaleditusuario', 'UserController@modalEditUsuario')
			->name('usuarios.modaleditusuario');

/* Ruta para mostrar la ventana modal del formulario BORRAR USUARIO
 * Se debe usar el metodo "POST" porque se necesita enviar el ID del
 * usuario a borrar. Se usa AJAX para enviar y recibir datos */
Route::post('/usuarios/modaldelusuario', 'UserController@modalDelUsuario')
			->name('usuarios.modaldelusuario');


/******************************************************************************
* RUTAS PARA GRABAR / MODIFICAR  REGISTROS QUE VENGAN DE LOS FORMULARIOS*/

// Ruta para GRABAR los datos que vienen del formulario modal AGREGAR USUARIO
Route::post('/usuarios/regUsuario','UserController@create')
			->name('usuarios.regUsuario');

// Ruta para ACTUALIZAR los datos que vienen del formulario EDITAR USUARIO
Route::post('/usuarios/editUsuario', 'UserController@update')
			->name('usuarios.editUsuario');

// Ruta para BORRAR los datos que vienen del formulario BORRAR USUARIO
Route::post('/usuarios/delUsuario', 'UserController@destroy')
			->name('usuarios.delUsuario');


/******************************************************************************
* RUTAS PARA PRESENTAR FORMULARIO DE ENVIO DE CORREOS Y TODO LO RELACIONADO*/

// Ruta para mostrar la creacion de emails
Route::get('/emails', 'MessagesController@index')
			->name('emails');

// Ruta para procesar el envio del correo
Route::post('/emails/send', 'MessagesController@store')
			->name('emails.send');