<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WelcomeUsersTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    // function test_welcome_users_with_nickname()
    // {
  		// $this->get('/saludo/angel/jr')
  	 //   		->assertStatus(200)
  	 //   		->assertSee("Bienvenido Angel, tu apodo es jr");
    
    // }

    // function test_welcome_users_without_nickname()
    // {
    
  		// $this->get('/saludo/Angel')
  	 //   		->assertStatus(200)
  	 //   		->assertSee("Bienvenido Angel");
    
    // }

    function test_welcome_users_without_nickname()
    {
    
      $this->get('/')
          ->assertStatus(200)
          ->assertSee("Dashboard");
    
    }
}
