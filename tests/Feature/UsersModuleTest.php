<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Models\User;

class UsersModuleTest extends TestCase
{
    //use RefreshDatabase; // este comando se usa para vaciar la base de datos para hacer la prueba en limpio... con los datos que le queramos meter dentro de la prueba.

    // function test_shows_default_message_if_users_list_is_empty()
    // {
    //     $this->withoutExceptionHandling();
        
    //     $this->get('/usuarios?empty')
    //             ->assertStatus(200)
    //             ->assertSee('No hay Usuarios Registrados.');
    // }

    // function test_it_loads_the_users_detail_page()
    // {
    //     $this->get('/usuarios/5')
    //     		->assertStatus(200)
    //     		->assertSee('Mostrando detalles del usuario Nro: 5');
    // }

    // function test_it_loads_the_new_users_page()
    // {
    //     $this->withoutExceptionHandling();

    //     $this->get('/usuarios/nuevo')
    //     		->assertStatus(200)
    //     		->assertSee('Crear nuevo usuario');
    // }

    function test_show_the_users_list_page()
    {
        $this->withoutExceptionHandling(); // para ver por consola cualquier error

        // factory(User::class)->create([
        //     'firstName' => 'Mariangel',
        //     'username' => 'soygigel'
        // ]);
        // factory(User::class)->create([
        //     'firstName' => 'Ana Monserrat',
           
        // ]);
        
        $this->get('/usuarios')
                ->assertStatus(200)
                ->assertSee('Listado de Usuarios')
                ->assertSee('Mariangel')
                ->assertSee('Ana Monserrat');
    }

    function test_create_a_new_user()
    {
        $this->withoutExceptionHandling(); // para ver por consola cualquier error

        $this->post('/usuarios/regUsuario', [
            'username'      => 'doblea71',
            'firstName'     => 'Angel',
            'lastName'      => 'Antunez',
            'email'         => 'doblea71@laravel.com',
            'address'       => 'Calle Principal la victoria',
            'phone'         => '424-987-32-12',
            'password'      => bcrypt('secret'),
            'isAdmin'       => '1',
            'website'       => 'elcuartodeangel.wordpress.com',
            'profession_id' => '1',
            'skill_id'      => '1'
        //]);
        //])->assertSee('registrando usuario'); // esto es para verificar que se vea este mensaje al hacer la prueba
        ])->assertRedirect(route('usuarios'));

        // verificar que el dato que acabamos de cargar esté presente en la base de datos
        $this->assertDatabaseHas('users', [
            'username'      => 'doblea71',
            'firstName'     => 'Angel',
            'lastName'      => 'Antunez',
            'email'         => 'doblea71@laravel.com',
            'address'       => 'Calle Principal la victoria',
            'phone'         => '424-987-32-12',
            'password'      => 'secret',
        ]);
    }

    function test_the_name_is_required()
    {
        $this->from('usuarios/regUsuario')->post('/usuarios', [
            'username'      => 'doblea71',
            'firstName'     => '',
        ])->assertSessionHasErrors(['firstName']);

        $this->assertDatabaseMissing('users', [
            'firstName' => 'Norberto',
        ]);
    }

    function test_loads_the_edit_user_page()
    {
        $this->get('/usuarios/editUsuario', ['idus' => $user->id])
                ->assertStatus(200)
                ->assertSee('Editar Usuario Nro:');
    }

    function test_deletes_a_user()
    {
        $user = factory(User::class)->create([
            'email' => 'admin@gmail.com'
        ]);

        $this->delete("usuarios/{$user->id}")
               ->assertRedirect('usuarios');

        $this->assertDatabaseMissing('user', [
            'id' => $user->id
        ]);

        //$this->asertSame(0, Usuarios::count());
    }
}
