<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
//use Illuminate\Support\Facades\DB;
use App\Http\Models\User as Usuarios; // Para usar todas las Funciones del Modelo "User"
use App\Http\Models\Profession as Profesiones; // Para usar todas las Funciones del Modelo "Profession"
use App\Http\Models\Skill_Model; // Para usar todas las Funciones del Modelo "Profession"

class MainController extends Controller
{
    public function index()
    {
    	//$data = array('1' => 'hola','2' => 'Mundo');
      //return view('front-end.dashboard');
      //return View::first(['plantillas.header', 'header'], $data);
      //return View::first(['front-end.dashboard', 'dashboard'], $data);
      //return View::make('plantillas.header')->nest('body','front-end.dashboard', $data)->nest('footer','plantillas.footer');

      /************************************************
      * Cargando datos especificos a la vista dashboard
      */
      /*$data = array(
        'cuantosUsuarios' => Usuarios::count(), 
        'cuantosProf' => Profesiones::count(),
        'cuantasHabil' => Skill_Model::count(),
      );
      $view = View::make('plantillas.header');
			$view->nest('body','front-end.dashboard', $data); // para mostrar el dashboard o index
			$view->nest('footer','plantillas.footer');
			return $view;*/

      /*********************************************************
      * Cargando varias vista usando @yield('content')
      */
      return view('front-end.dashboard')
                ->with('cuantosUsuarios', Usuarios::count())
                ->with('cuantosProf', Profesiones::count())
                ->with('cuantasHabil', Skill_Model::count())
                ->with('liactive','dashboard'); // otra forma de enviar datos a la vista



    }
}
