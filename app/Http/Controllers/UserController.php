<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
//use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Pagination\Paginator;
use App\Http\Models\User as Usuarios;  // Para usar todas las Funciones del Modelo "User"
use App\Http\Models\Profession as Prof; // llamo para que funcionen los metodos (funciones) del Modelo Profession
use App\Http\Models\Skill_Model as Skill; // llamo para que funcionen los metodos (funciones) del Modelo Skill_Model

class UserController extends Controller
{
    /*public function index()
    {
        if (request()->has('empty')) {
            $users = [];
        } else {
            $users = [
                    'Moises',
                    'Rossangel',
                    'Mariangel',
                    'Ana Monserrat',
                    '<script>alert("Hola")</script>'
                ];
        }

        $title = 'Listado de Usuarios';

        /*return view('usuarios',[
            'users' => $users,
            'title' => 'Listado de Usuarios'
        ]);*/

        /*return view('usuarios')
            ->with('users',$users)
            ->with('title',$title);

        //return 'Usuarios';

        //return view('usuarios', compact('title','users'));

        //return view('users', compact('title','users'));
    }*/
    /*public function show($id)
    {
        //Aqui puedo usar el modelo para buscar al usuario y si no lo consigue muestra una vista de error 404
        $user = Usuarios::find($id);
        if($user == null) {
            return response()->view('front-end.errors.error_404',[],404); //view(pagina blade, valores enviados, statusDepagina)
        }

        //$user = Usuarios::findOrFail($id); // esta funcion es propia de laravel para manejar  errores
    }*/
    /*public function index()
    {
        if (request()->has('empty')) {
            $users = [];
            $title = 'Listado de Usuarios';
        } else {
            /************************************
             * Usando el Facade DB para manejar solo MySQL
             */
            //$users = DB::table('users')->get(); // usando directamente MySQL
            /*$users = DB::table('users')
                        ->leftJoin('professions', 'users.profession_id', '=', 'professions.id')
                        ->get();*/

            /************************************
             * Usando Eloquent de forma directa
             */
            //$users = Usuarios::usuarioProfesion(); //usuarioProfesion es una funcion en el Modelo "User"

            /*********************************************************************************
             * Usando Eloquent, declarando la relacion por "Clave Foranea" en el modelo "User"
             * (funcion "profession()") y declarando el "hasMany" dentro de una funcion del
             * modelo "Profession"
             */
            //$users = Usuarios::with('profession')->get();
            //$title = 'Listado de Usuarios';
        //}

        // si traigo los datos desde la BD y no hay datos
        //$query = Usuarios::with('profession')->get();
        /*if($query->count() <= 0) {
            $users = [];
            $title = 'Listado de Usuarios';
        } else {*/
            /************************************
             * Usando el Facade DB para manejar solo MySQL
             */
            //$users = DB::table('users')->get(); // usando directamente MySQL
            /*$users = DB::table('users')
                        ->leftJoin('professions', 'users.profession_id', '=', 'professions.id')
                        ->get();*/

            /************************************
             * Usando Eloquent de forma directa
             */
            //$users = Usuarios::usuarioProfesion(); //usuarioProfesion es una funcion en el Modelo "User"

            /*********************************************************************************
             * Usando Eloquent, declarando la relacion por "Clave Foranea" en el modelo "User"
             * (funcion "profession()") y declarando el "hasMany" dentro de una funcion del
             * modelo "Profession"
             */
            //$users = Usuarios::with('profession')->get();
            //$title = 'Listado de Usuarios';
        //}

        //dd($users);  // el dd sirve como un "print_r"
        //return view('users', compact('title','users')); // mostrando una vista sencilla...
        /*return view('user.index')
                ->with('users', Usuarios::all())
                ->with('title', 'Listado de Usuarios');*/ // otra forma de enviar datos a la vista

        /********************************************
        * Cargar varias vistas ("header - body - footer") por separado
        * para que solo sea posible cargar el body dinamicamente
        */
        /*$data = compact('title','users');
        $view = View::make('plantillas.header');
        $view->nest('body','front-end.usuarios.usuarios', $data); // muestra una pagina de prueba
        $view->nest('footer','plantillas.footer');
        return $view;*/

        /******************************************************
        * Cargando una sola vista y usando @yield('content')
        *
        return view('front-end.usuarios.usuarios')
                ->with('users', Usuarios::all())
                ->with('title', 'Listado de Usuarios')
                ->with('liactive','usuarios'); // otra forma de enviar datos a la vista
    }*/
    /*public function create()
    {
        return "Crear nuevo usuario";
    }*/
    /*public function show($id)
    {
        return view('front-end.usuarios.editUsuario')
                ->with('users', Usuarios::editUsuario($id))
                ->with('profes', Prof::all())
                ->with('skills', Skill::all())
                ->with('title', "Editar Usuario Nro: {$id}")
                ->with('liactive','usuarios'); // otra forma de enviar datos a la vista
    }*/

    public function findOrFail($id, $columns = ['*'])
    {
        $result = $this->find($id, $columns);
    }

    public function index()
    {
        // consulto la BD con sus relaciones
        $query = Usuarios::with('profession')->get();
        // si traigo los datos desde la BD y no hay datos
        if (request()->has('empty') || $query->count() <= 0) {
            $users = [];
            $title = 'Listado de Usuarios';
        } else {
            /*********************************************************************************
             * Usando Eloquent, declarando la relacion por "Clave Foranea" en el modelo "User"
             * (funcion "profession()") y declarando el "hasMany" dentro de una funcion del
             * modelo "Profession"
             */
            /******************************************************
            * Cargando una sola vista y usando @yield('content')
            */
            $users = Usuarios::with('profession')
                                ->orderBy('id', 'DESC')
                                ->paginate(5);
            $title = 'Listado de Usuarios';
            return view('front-end.usuarios.usuarios')
                    ->with('users', $users)
                    ->with('title', $title); // otra forma de enviar datos a la vista
        }
    }

    // mostrar el formulario registro
    public function modalRegUsuario()
    {
        return view('front-end.usuarios.regUsuarioModal')
                ->with('profes', Prof::all())
                ->with('skills', Skill::all())
                ->with('title', "Registrar Usuario"); // otra forma de enviar datos a la vista
    }

    // mostrar el formulario editar
    public function modalEditUsuario(Request $request)
    {
        //$idus = $request->all(); // Usar esto asi es peligroso, mejor recibir una variable mas especifica
        $idus = ['idus' => $request->idus]; // request busca el valor de la variable segun el nombre que le hayamos dado al enviarla para aca.
        return view('front-end.usuarios.editUsuarioModal')
                ->with('users', Usuarios::editUsuario($idus))
                ->with('profes', Prof::all())
                ->with('skills', Skill::all())
                ->with('title', "Editar Usuario Nro: {$idus['idus']}"); // otra forma de enviar datos a la vista
    }
    // mostrar el formulario editar
    public function modalDelUsuario(Request $request)
    {
        //$idus = $request->all(); // Usar esto asi es peligroso, mejor recibir una variable mas especifica
        $idus = ['idus' => $request->idus]; // request busca el valor de la variable segun el nombre que le hayamos dado al enviarla para aca.

        return view('front-end.usuarios.delUsuarioModal')
                ->with('users', Usuarios::findById($idus))
                ->with('title', "Borrar Usuario Nro: {$idus['idus']}"); // otra forma de enviar datos a la vista
    }

    // grabar registro de nuevo usuario
    public function create()
    {
        // con request->all() puedo recibir todos los post enviados a esta funcion
        //$data = request->all();
        /* Con request()->validate() puedo validar e incluso mostrar un mensaje de error
         * directamente con laravel para inyectarlo al formulario de donde recibo los datos
         * primero se pasan los post que son reuqerido y luego se definen los mensajes a mostrar*/
        $data = request()->validate([
            'firstName'     => 'required',
            'lastName'      => 'required',
            'username'      => 'required',
            'password'      => 'required',
            'email'         => 'required|email|unique:users,email',
            'address'       => 'required',
            'website'       => 'required',
            'phone'         => 'required',
            'profession_id' => 'required',
            'skill_id'      => 'required',
            'isAdmin'       => 'required',
        ], [
            'firstName.required'     => 'El campo firstName es obligatorio.',
            'lastName.required'      => 'El campo lastName es obligatorio.',
            'username.required'      => 'El campo username es obligatorio.',
            'password.required'      => 'El campo password es obligatorio.',
            'email.required'         => 'El campo email es obligatorio.',
            'address.required'       => 'El campo address es obligatorio.',
            'website.required'       => 'El campo website es obligatorio.',
            'phone.required'         => 'El campo phone es obligatorio.',
            'profession_id.required' => 'El campo profession_id es obligatorio.',
            'skill_id.required'      => 'El campo skill_id es obligatorio.',
            'isAdmin.required'       => 'El campo isAdmin es obligatorio.',
        ]);
        //dd($data);

       // Con *::create() puedo GUARDAR directamente a la Base de datos sin mayores complicaciones
        Usuarios::create([
            'firstName'     => $data['firstName'],
            'lastName'      => $data['lastName'],
            'username'      => $data['username'],
            'password'      => bcrypt($data['password']),
            'email'         => $data['email'],
            'address'       => $data['address'],
            'website'       => $data['website'],
            'phone'         => $data['phone'],
            'profession_id' => $data['profession_id'],
            'skill_id'      => $data['skill_id'],
            'isAdmin'       => $data['isAdmin'],
        ]);

        //return 'registrando usuario';
        return redirect()->route('usuarios'); // redirije hacia la vista del listado de usuarios despues de registrar el nuevo dato
    }

    // grabar modificacion/edicion de un usuario
    public function update()
    {
        // con request->all() puedo recibir todos los post enviados a esta funcion
        //$data = $request->all();
        /* Con request()->validate() puedo validar e incluso mostrar un mensaje de error
         * directamente con laravel para inyectarlo al formulario de donde recibo los datos
         * primero se pasan los post que son reuqerido y luego se definen los mensajes a mostrar*/
        $data = request()->validate([
            'id'            => 'required',
            'firstName'     => 'required',
            'lastName'      => 'required',
            'username'      => 'required',
            'password'      => 'sometimes', // para validar solo cuando viene con datos
            //'email'         => 'required|email|unique:users,email,'.request('id'),
            'email'         => [
                'required',
                'email',
                Rule::unique('users')->ignore(request('id'))
            ], // Orientada a Objetos, funciona igual. Con esta regla hacemos que el email se mantenga siendo unico pero que ignore verificar el correo del usuario que intentamos editar
            'address'       => 'required',
            'website'       => 'required',
            'phone'         => 'required',
            'profession_id' => 'required',
            'skill_id'      => 'required',
            'isAdmin'       => 'required',
        ]);

        $id = request('id');

        // debigging
        //dd($data);

        //luego de validar el campo ahora lo encripto para actualizarlo en la bd
        if ( $data['password'] != null || $data['password'] != '') {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']); // para quitar este valor del array asociativo "$data"
        }

        // de esta forma hacems uso e la funcion para actualizar creada en el modelo User
        //Usuarios::updateData(request('id'),$data);

        // Con $user->update($data) laravel se encarga de ACTUALIZAR en la Base de datos al ID que se està manipulando. QUE MARAVILLA..!
        Usuarios::updateData($id, $data);
        //return redirect()->route('usuarios', ['user' => $user]);
        return redirect()->route('usuarios');
    }

    public function destroy()
    {
        //dd(request());
        $id  = request('id');
        $del = request('borrar_id');

        if($del == 'Si'){

            Usuarios::destroyData($id);
            return redirect()->route('usuarios');

        } else {

            return redirect()->route('usuarios');
        }
    }
}
