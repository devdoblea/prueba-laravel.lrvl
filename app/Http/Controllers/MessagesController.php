<?php

namespace App\Http\Controllers;

use App\Mail\MensajeRecibido;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MessagesController extends Controller
{
		public function index()
		{
			$title = 'Envio de Correos Electrónicos';
      return view('front-end.emails.emails')
              ->with('title', $title); // otra forma de enviar datos a la vista
		}
    public function store()
    {
    	$mensaje = request()->validate([
    		'firstName' => 'required',
    		'email'     => 'required|email',
    		'subject'   => 'required',
    		'content'   => 'required|min:3',
    	], [
    		'firstName.required' => __('Necesito saber tu Nombre'),
    	]);

    	// enviar el email para el administrador
    	//Mail::to('devdoblea@gmail.com')->send(new MensajeRecibido($mensaje));
    	Mail::to('devdoblea@gmail.com')->queue(new MensajeRecibido($mensaje)); // envios de datos en segundo plano

    	//return $mensaje;
    	return 'Mensaje Enviado';
    	//return new MensajeRecibido($mensaje); // asi puedo ver los datos enviados al correo en el navegador
    }
}
