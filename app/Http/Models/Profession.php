<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
    // protected $table = 'profession'; // esto se usa en caso de querer trabajar en una tabla diferente de la que usa laravel por convencion: tabla de base de datos = a nombre del archivo contentivo del Modelo de esa tabla.

		protected $fillable = ['title'];// para cargar columnas de forma Masiva

		public function users()
		{
			return $this->hasMany(User::class);
		}
}
