<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; // Esto para generar consultas con Eloquent
use Illuminate\Database\Eloquent\Collection; // Esto para generar consultas con Eloquent
use Illuminate\Support\Facades\DB;      // Esto para generar consultas con MySQL


class User extends Model
{
    // protected $table = 'user'; // esta variable protegida se usa en caso de querer trabajar en una tabla diferente de la que usa laravel por convencion: tabla de base de datos = a nombre del archivo contentivo del Modelo de esa tabla.

	protected $fillable = [
        'username','firstName', 'lastName', 'email', 'address', 'phone', 'password', 'isAdmin', 'website', 'profession_id', 'skill_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $cast = [
        'isAdmin' => 'boolean',
    ];

    public static function findByEmail($email)
    {
        return static::where(compact('email'))->first();
        //return User::where(compact('email'))->first(); // Debe funcionar igual que el de arriba porque estamos dentro del metodo User
    }

    // va a la tabla profession y busca el campo profession_id para hacer la relacion
    public function profession()
    {
    	return $this->belongsTo(Profession::class);
    }

    // va a la tabla profession y busca el campo profession_id para hacer la relacion
    public function skill()
    {
        return $this->belongsTo(Skill_Model::class);
    }

    // Sirve para comparar un usuario para verifiacr si es ADMIN o no
    public function isAdmin()
    {
        return $this->email === "rosa@admin.com";
    }

    //para saber cual es el nombre de la profesion
    public static function whatProfession($id)
    {
        return static::where('id')->value('title');
    }

    //todos los usuarios con su nombre de profesion
    public static function usuarioProfesion()
    {
        $users = DB::table('users')
                    ->leftJoin('professions', 'users.profession_id', '=', 'professions.id')
                    ->get();
        return $users;
    }

    public static function findById($id)
    {
        return static::where('id',$id)
                        ->with('profession')
                        ->with('skill')
                        ->get();
    }

    public static function editUsuario($id)
    {
        $dato = static::where('id',$id)
                        ->with('profession')
                        ->with('skill')
                        ->get();
        return $dato;
    }

    public static function updateData($id,$data)
    {
        return static::where('id',$id)->update($data);
    }

    public static function destroyData($id)
    {
        return static::where('id',$id)->delete();
    }


}
