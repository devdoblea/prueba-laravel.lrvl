<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Skill_Model extends Model
{
    protected $table = 'skill';
    protected $fillable = ['skill'];// para cargar columnas de forma Masiva

		public function users()
		{
			return $this->hasMany(User::class);
		}
}
