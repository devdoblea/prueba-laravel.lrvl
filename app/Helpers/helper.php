<?php

	function setActiveHome($routeName)
	{
		return request()->is($routeName) ? 'active' : '';
	}

	function setActive($routeName)
	{
		return request()->routeIs($routeName) ? 'active' : '';
	}
